//
//  ViewController.swift
//  MultithreadingTest
//
//  Created by Роман Хоменко on 13.10.2021.
//

// - MARK: app with random pics after pressing the button
// - TODO: array with pics url, try use group queues

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ImageA: UIImageView!
    @IBOutlet weak var ImageB: UIImageView!
    @IBOutlet weak var ImageC: UIImageView!
    @IBOutlet weak var ImageD: UIImageView!
    @IBOutlet weak var ImageE: UIImageView!
    @IBOutlet weak var ImageF: UIImageView!
    @IBOutlet weak var ButtonA: UIButton!
    
    let imageURLs = [
        "https://memepedia.ru/wp-content/uploads/2021/05/amogus.jpg",
        "https://www.planetware.com/photos-large/F/france-paris-eiffel-tower.jpg",
        "https://upload.wikimedia.org/wikipedia/ru/thumb/4/4d/Wojak.png/200px-Wojak.png",
        "https://memepedia.ru/wp-content/uploads/2021/05/amogus.jpg",
        "https://www.planetware.com/photos-large/F/france-paris-eiffel-tower.jpg",
        "https://upload.wikimedia.org/wikipedia/ru/thumb/4/4d/Wojak.png/200px-Wojak.png"
    ]
    var arrayImages = [UIImage]()
    var viewImages = [UIImageView]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
//        settingStandartGifs()
        asyncURLSession()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
//        downloadURLPhoto()
//        asyncGroup()
//        asyncURLSession()
        settingStandartGifs()
        ButtonA.addTarget(self, action: #selector(refreshImage), for: .touchUpInside)
    }
    
    func settingStandartGifs() {

        viewImages.append(ImageA)
        viewImages.append(ImageB)
        viewImages.append(ImageC)
        viewImages.append(ImageD)
        viewImages.append(ImageE)
        viewImages.append(ImageF)
        
        for i in 0...viewImages.count - 1 {
            viewImages[i].loadGif(name: "gifForProject")
        }
    }
    
    func downloadURLPhoto() {
        guard let imageURL = URL(string: "https://www.planetware.com/photos-large/F/france-paris-eiffel-tower.jpg") else { return }
        let queue = DispatchQueue.global(qos: .utility)
        queue.async {
            if let imageData = try? Data(contentsOf: imageURL) {
                sleep(3)
                DispatchQueue.main.async {
                    self.ImageA.image = UIImage(data: imageData)
                }
            }
        }
    }
    
    func asyncGroup() {
        let group = DispatchGroup()
        
        for i in 0...5 {
            group.enter()
            asyncLoadImage(imageURL: URL(string: imageURLs[i])!,
                           runQueue: .global(),
                           completionQueue: .main) { result, error in
                if let image = result { self.arrayImages.append(image) }
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            for i in 0...self.arrayImages.count - 1 {
                self.viewImages[i].image = self.arrayImages[i]
            }
        }
    }
    
    func asyncURLSession() {
        for i in 0...5 {
            let url  = URL(string: imageURLs[i])
            let request = URLRequest(url: url!)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                DispatchQueue.main.async {
                    self.arrayImages.append(UIImage(data: data!)!)
                    self.viewImages[i].image = UIImage(data: data!)
                }
            }
            task.resume()
        }
    }
    
    // - MARK: should refresh UIImageView with new images on its positions
    @objc func refreshImage() {
        let firstQueue = DispatchQueue.global(qos: .utility)
        let secondQueue = DispatchQueue.global(qos: .utility)
        
        firstQueue.async {
            
            for i in 0...5 {
                self.asyncLoadImage(imageURL: URL(string: self.imageURLs[i])!,
                               runQueue: .global(),
                               completionQueue: .main) { result, error in
                    if let image = result { self.arrayImages.append(image) }
                }
            }
        }
        
        secondQueue.async {
            DispatchQueue.main.async {
                print(self.arrayImages.count)
                print(self.viewImages.count)
                for _ in 0...5 {
                    self.viewImages[Int.random(in: 0...5)].image = self.arrayImages[Int.random(in: 0...5)]
                }
                self.arrayImages = []
            }
        }
    }
}
    

extension ViewController {
    func asyncLoadImage(imageURL: URL,
                        runQueue: DispatchQueue,
                        completionQueue: DispatchQueue,
                        completion: @escaping (UIImage?, Error?) -> ()) {
        runQueue.async {
            do {
                let data = try Data(contentsOf: imageURL)
                
                completionQueue.async {
                    completion(UIImage(data: data), nil)
                }
            } catch let error {
                completionQueue.async {
                    completion(nil, error)
                }
            }
        }
    }
}
